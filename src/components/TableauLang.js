const tableauLang = [
    {name : 'HTML', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/w3_html5-icon_odnmne.svg"},
    {name : 'CSS', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/w3_css-icon_jovqqn.svg"},
    {name : 'JavaScript', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/Unofficial_JavaScript_logo_2.svg_aduj02.png"},
    {name : 'TypeScript', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1659269942/Portfolio/typescript_ohappe.png"},
    {name : 'NodeJS', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/nodejs-icon_rdglhm.svg"},
    {name : 'ExpressJS', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/expressjs-icon_zmwltx.svg"},
    {name : 'NextJS', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1659269942/Portfolio/nextjs_ykzrua.png"},
    {name : 'ReactJS', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/reactjs-icon_trahiz.svg"},
    {name : 'Redux', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/redux-original_wkqbfm.svg"},
    {name : 'MongoDB', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/mongodb-icon_sepblf.svg"},
    {name : 'Heroku', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/heroku-icon_sszcjk.svg"},
    {name : 'GitHub', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/github-tile_uhagb9.svg"},
    {name : 'GitLab', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1659269942/Portfolio/gitlab_ntjg1p.png"},
    {name : 'Git', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/git-scm-icon_ikqo9r.svg"},
    {name : 'Postman', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734029/Portfolio/getpostman-icon_rtuf5u.svg"},
    {name : 'Expo', src : "https://res.cloudinary.com/dktfcexev/image/upload/v1655734027/Portfolio/expoio-icon_enleuo.svg"},
]

export default tableauLang;
